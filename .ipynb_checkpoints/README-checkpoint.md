# SQL Structured Query Language
===========================================


'''
                         ___      
                        /\_ \     
  ____         __       \//\ \    
 /',__\      /'__`\       \ \ \   
/\__, `\    /\ \L\ \       \_\ \_ 
\/\____/    \ \___, \      /\____\
 \/___/      \/___/\ \     \/____/
                  \ \_\                     
                   \/_/               

'''


Coding examples for the SQL programming language. Some of the examples can be directly loaded into a PostgreSQL database via an interactive shell, others use Python Jupyter notebooks to test and create automations scripts.

General concepts for building tables, select statements, joins and inserts are covered in notebooks that begin with two zeros.

Data sets are chosen for their cross platform compatibility, you should be able to load them into PostgreSQL, Oracle and SQLite3.   I have included spreadsheets and a Python module called Faker to generate high quality testing data sets.  

##Links to SQL code sources:
------------------------------------

+[Learning PostgreSQL](https://www.packtpub.com/big-data-and-business-intelligence/learning-postgresql)

+[SQL Pocket Guide by Jonathan Gennick](https://www.oreilly.com)

+[Sam's Teach Yourself SQL](https://www.oreilly.com/library/view/sams-teach-yourself/9780132603911/)
 
+[Dave Berry : Pluralsight](https://www.pluralsight.com/authors/david-berry)



##Guide to files in this repository
--------------------------------------


I have some SQLite3 examples in this 00_mt_SQLite_builds.ipynb, This assumes that you have installed the default configuration of Anaconda.  SQLite3 schema contain basic data types, dates are stored a text columns.  The cursor examples will work on a local host, multi-system transactions will require additional network configurations.


Some basic Oracle based code examples are included in 00_mt_SQL_Oracle_core_builds.ipynb, you should be able to paste these code blocks into SQL*Develper.


This notebook will create the output files prospective_students_import_oracle.sql, treasure_map_yorders.sql.  You should be able to paste these into an IDE console or terminal shell.



PostgreSQL examples that cover the basics are covered in 00_mt_PostgreSQL_core_builds.ipynb.  This notebook will create the output files mt_botanica_shema.sql, pg_test_conn.sh. 

Create schema, insert data and query for it, 



 
